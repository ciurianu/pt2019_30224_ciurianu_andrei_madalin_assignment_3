package Panels;

import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import Connection.MyConnection;
import Models.Customer;
import Models.Product;
import javafx.scene.layout.Border;

public class ViewProductOrder {
	
	Connection connection = null;
    ResultSet rs = null;
    PreparedStatement ps = null;
	private JFrame frame;
	private JTable table;
	private JButton back;
	private JButton finish;
	private JLabel cantitate = new JLabel("Cantitate ");
	private JLabel empty = new JLabel();
	private JTextField cantitateText = new JTextField("");
    private Customer customer;
    private int idCustomer;
    private Product product;
    private int idProduct;
    DefaultTableModel defaultTableModel = new DefaultTableModel();
    String data;
    
    public ViewProductOrder(Customer customer,int id) {
    	this.customer = customer;
    	this.idCustomer = id;
    	
    	frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(3,2));
	    frame.setVisible(true);
	    back = new JButton("Back");
	    finish = new JButton("Finish!");
	    back.setBounds(0, 10, 0, 22);
	
		Object columns[] = {"Id","Nume","Pret","Stoc"};
        defaultTableModel.setColumnIdentifiers(columns);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                TableMouseClicked(evt);
            }
        });
		
		JScrollPane scrollPane = new JScrollPane();
		table.setModel(defaultTableModel);
		scrollPane.setViewportView(table);
		
		frame.getContentPane().add(scrollPane);
		frame.add(empty);
		frame.add(cantitate);
		frame.add(cantitateText);
        frame.add(back);
        frame.add(finish);
        
        loadData();
        backListener();
        finishListener();
    
        
    }
	
	
	 public void loadData() {
	        connection = MyConnection.ConnectDb();
	        defaultTableModel.getDataVector().removeAllElements();
	        defaultTableModel.fireTableDataChanged();
	        String sql = "select * from Product";
	        try {
	            ps = connection.prepareStatement(sql);
	            rs = ps.executeQuery();
	            Object columnData[] = new Object[6];
	            while (rs.next()) {
	                columnData[0] = rs.getInt("product_id");
	                columnData[1] = rs.getString("descriere");
	                columnData[2] = rs.getFloat("pret");
	                columnData[3] = rs.getInt("stoc");
	                defaultTableModel.addRow(columnData);

	            }
	        } catch (SQLException e) {
	            JOptionPane.showMessageDialog(null, e);
	        }
	    }
	 
	public void backListener() {
		  back.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					frame.setVisible(false);
					new ViewCustomerOrder();
				}
			});
	}
	
	 private void TableMouseClicked(MouseEvent evt) {
	        int row = table.getSelectedRow();
	        idProduct = Integer.parseInt(table.getValueAt(row, 0).toString());
	        String nume = table.getValueAt(row, 1).toString();
	        float pret = Float.parseFloat(table.getValueAt(row, 2).toString());
	        int stoc = Integer.parseInt(table.getValueAt(row, 3).toString());
	        
	        product = new Product(nume, pret, stoc);
	        
	       
	    }

	 public void finishListener() {
		 finish.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					int cantitate = Integer.parseInt(cantitateText.getText());
					
					if(cantitate > product.getStoc()) {
						JOptionPane.showMessageDialog(null,"Cantitate prea mare!");
					}
					else {
						float pretComanda = product.getPret()*cantitate;
						int stocFinal = product.getStoc() - cantitate;
						connection = MyConnection.ConnectDb();			        
						String sql = "Update Product set descriere = '" + product.getNume() +"' , pret = '" + product.getPret() +"' , stoc = '" + stocFinal + "' where product_id = '" + idProduct + "'";
						try {
				            ps = connection.prepareStatement(sql);
				            ps.execute();
				            defaultTableModel.getDataVector().removeAllElements();
				            defaultTableModel.fireTableDataChanged();
				            loadData();
				        } catch (HeadlessException | SQLException e1) {
				            JOptionPane.showMessageDialog(null, e1);
				        }      
						
				        sql = "insert into Orders(pretComanda, cantitate, customer_id,product_id) values (?,?,?,?)";
				        try {
				            ps = connection.prepareStatement(sql);
				            ps.setFloat(1, pretComanda);
				            ps.setInt(2, cantitate);
				            ps.setInt(3, idCustomer);
				            ps.setInt(4, idProduct);
				            ps.execute();
				            JOptionPane.showMessageDialog(null, "Data Saved");
				            
				        } catch (SQLException e1) {
				            JOptionPane.showMessageDialog(null, e1);
				        }
				        
				        
					}
				}
			});
	 }
}
