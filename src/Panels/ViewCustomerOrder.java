package Panels;

import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Connection.MyConnection;
import Models.Customer;
import javafx.scene.layout.Border;

public class ViewCustomerOrder extends JPanel{
	
	Connection connection = null;
    ResultSet rs = null;
    PreparedStatement ps = null;
	private JFrame frame;
	private JTable table;
	private JButton back;
	private JButton next;
	private int idCustomer;
	private Customer customer;
    
    DefaultTableModel defaultTableModel = new DefaultTableModel();
    String data;
    
    public ViewCustomerOrder() {
    	
    	frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setVisible(true);
	    setLayout(new GridLayout(3,1));
	    next = new JButton("Next");
	    back = new JButton("Back");
	    back.setBounds(0, 10, 0, 22);
		Object columns[] = {"Id","Nume","Prenume","Email","Adresa"};
        defaultTableModel.setColumnIdentifiers(columns);
		
		table = new JTable();
		
		table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                TableMouseClicked(evt);
            }
        });
		JScrollPane scrollPane = new JScrollPane();
		table.setModel(defaultTableModel);
		scrollPane.setViewportView(table);
		
		add(scrollPane);
        add(back);
        add(next);
        frame.add(this);
        loadData();
        backListener();
        nextListener();
    
        
    }
	
	
	 public void loadData() {
	        connection = MyConnection.ConnectDb();
	        defaultTableModel.getDataVector().removeAllElements();
	        defaultTableModel.fireTableDataChanged();
	        String sql = "select * from Customer";
	        try {
	            ps = connection.prepareStatement(sql);
	            rs = ps.executeQuery();
	            Object columns[] = {"Id","Nume","Prenume","Email","Adresa"};
	            defaultTableModel.setColumnIdentifiers(columns);
	            Object columnData[] = new Object[6];
	            while (rs.next()) {
	                columnData[0] = rs.getInt("customer_id");
	                columnData[1] = rs.getString("nume");
	                columnData[2] = rs.getString("prenume");
	                columnData[3] = rs.getString("email");
	                columnData[4] = rs.getString("adresa");
	                defaultTableModel.addRow(columnData);

	            }
	        } catch (SQLException e) {
	            JOptionPane.showMessageDialog(null, e);
	        }
	    }
	 
	public void backListener() {
		 back.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					frame.setVisible(false);
					
				}
			});
	}
	
	public void nextListener() {
		 next.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					frame.setVisible(false);
					new ViewProductOrder(customer,idCustomer);
					
				}
			});
	}
	
	 private void TableMouseClicked(MouseEvent evt) {
	        int row = table.getSelectedRow();
	        idCustomer = Integer.parseInt(table.getValueAt(row, 0).toString());
	        String nume = table.getValueAt(row, 1).toString();
	        String prenume = table.getValueAt(row, 2).toString();
	        String email = table.getValueAt(row, 3).toString();
	        String adresa =table.getValueAt(row, 4).toString();
	        customer = new Customer(nume, prenume, email, adresa);
	       
	 }
}
