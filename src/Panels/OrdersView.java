package Panels;

import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Connection.MyConnection;
import javafx.scene.layout.Border;

public class OrdersView {
	
	Connection connection = null;
    ResultSet rs = null;
    PreparedStatement ps = null;
	private JFrame frame;
	private JTable table;
	private JButton back;
    
    DefaultTableModel defaultTableModel = new DefaultTableModel();
    String data;
    
    public OrdersView() {
    	
    	frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(2,0));
	    frame.setVisible(true);
	    back = new JButton("Back");
	    back.setBounds(0, 10, 0, 22);
	    back.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frame.setVisible(false);
				
			}
		});
		Object columns[] = {"Id","PretComanda","CantitateComandata","Nume","Prenume","NumeProdus","Pret"};
        defaultTableModel.setColumnIdentifiers(columns);
		
		table = new JTable();
		
		
		JScrollPane scrollPane = new JScrollPane();
		table.setModel(defaultTableModel);
		scrollPane.setViewportView(table);
		
		frame.getContentPane().add(scrollPane);
        frame.add(back);
        loadData();
    
        
    }
	
	
	 public void loadData() {
	        connection = MyConnection.ConnectDb();
	        defaultTableModel.getDataVector().removeAllElements();
	        defaultTableModel.fireTableDataChanged();
	        String sql = "select * from Orders";
	        try {
	            ps = connection.prepareStatement(sql);
	            rs = ps.executeQuery();
	            Object columnData[] = new Object[7];
	            while (rs.next()) {
	                columnData[0] = rs.getInt("order_id");
	                columnData[1] = rs.getFloat("pretComanda");
	                columnData[2] = rs.getInt("cantitate");
	                columnData[3] = rs.getInt("customer_id");
	                columnData[5] = rs.getInt("product_id");
	                columnData = getClientData(Integer.parseInt(columnData[3].toString()), columnData);
	                columnData = getProductData(Integer.parseInt(columnData[5].toString()),columnData);
	                defaultTableModel.addRow(columnData);

	            }
	        } catch (SQLException e) {
	            JOptionPane.showMessageDialog(null, e);
	        }
	    }
	 
	 public Object[] getClientData(int clientId,Object[] columnData) {
	        String sql = "select nume,prenume from Customer where customer_id = '"+clientId+"'";
	        PreparedStatement ps2;
	        ResultSet rs2;
	        try {
	            ps2 = connection.prepareStatement(sql);
	            rs2 = ps2.executeQuery();
	            while (rs2.next()) {
	                columnData[3] = rs2.getString("nume");
	                columnData[4] = rs2.getString("prenume");
	       
	            }
	        } catch (SQLException e) {
	            JOptionPane.showMessageDialog(null, e);
	        }
	        return columnData;
		 
	 }
	 public Object[] getProductData(int productId,Object[] columnData) {
	
	        String sql = "select descriere,pret from Product where product_id = '"+productId+"'";
	        PreparedStatement ps2;
	        ResultSet rs2;
	        try {
	            ps2 = connection.prepareStatement(sql);
	            rs2 = ps2.executeQuery();
	            while (rs2.next()) {
	                columnData[5] = rs2.getString("descriere");
	                columnData[6] = rs2.getFloat("pret");
	           
	            }
	        } catch (SQLException e) {
	            JOptionPane.showMessageDialog(null, e);
	        }
	        return columnData;
	 }
	 
	

}
