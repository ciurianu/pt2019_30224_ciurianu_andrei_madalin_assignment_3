package MainController;

import javax.swing.JOptionPane;

import com.sun.javafx.geom.AreaOp.AddOp;

import Panels.AddCustomer;
import Panels.AddProduct;
import Panels.CustomerView;
import Panels.DeleteCustomer;
import Panels.EditCustomer;
import Panels.OrdersView;
import Panels.RemoveProduct;
import Panels.UpdateStock;
import Panels.ViewCustomerOrder;
import Panels.WarehouseView;



public class MainController {
	
	MainPanel mainP;
	public MainController() {
		mainP = new MainPanel();
		
		initializeActionListeners();
	
	}
	
	
	
	private void initializeActionListeners() {
			
			
		initializeViewCustomers();
		initializeAddCustomers();
		initializeEditCustomers();
		initializeDeleteCustomers();
		initializeViewWarehouse();
		initializeViewOrders();
		initializeAddProduct();
		initializeRemoveProduct();
		initializeUpdateStock();
		initializeAddOrder();
		
		}
	
	private void initializeViewCustomers() {
		mainP.viewCustomers(e->{
			new CustomerView();
		});
	}
	private void initializeAddCustomers() {
		mainP.addCustomer(e->{
			new AddCustomer();
		});
	}
	private void initializeEditCustomers() {
		mainP.editCustomer(e->{
			new EditCustomer();
		});
	}
	private void initializeDeleteCustomers() {
		mainP.deleteCustomer(e->{
			new DeleteCustomer();
		});
	}
	private void initializeViewWarehouse() {
		mainP.viewWarehouse(e->{
			new WarehouseView();
		});
	}
	private void initializeViewOrders() {
		mainP.viewOrders(e->{
			new OrdersView();
		});
	}
	private void initializeAddProduct() {
		mainP.addProduct(e->{
			new AddProduct();
		});
	}
	private void initializeRemoveProduct() {
		mainP.removeProduct(e->{
			new RemoveProduct();
		});
	}
	private void initializeUpdateStock() {
		mainP.updateStock(e->{
			new UpdateStock();
		});
	}
	private void initializeAddOrder() {
		mainP.addOrder(e->{
			new ViewCustomerOrder();
		});
	}

}
