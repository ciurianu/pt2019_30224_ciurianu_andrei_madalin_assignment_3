package Panels;


import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Connection.MyConnection;
import Models.Customer;

public class AddCustomer extends JPanel {
	

	private JLabel instruction = new JLabel("Complete your profile");
	private JLabel empty = new JLabel("");
	private JLabel name = new JLabel("Nume");
	private JTextField nameText = new JTextField();
	private JLabel prenume = new JLabel("Prenume");
	private JTextField prenumeText = new JTextField();
	private JLabel address = new JLabel("Address");
	private JTextField addressText = new JTextField();
	private JLabel email = new JLabel("Email");
	private JTextField emailText = new JTextField();
	private JButton back = new JButton("Back");
	private JButton save = new JButton("Save");
	private JFrame frame;
	Connection connection = null;
    PreparedStatement ps = null;

		public  AddCustomer() {
			frame = new JFrame();
			frame.setBounds(100, 100, 450, 300);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			 frame.setVisible(true);
			 frame.add(this);
			setLayout(new GridLayout(6, 2));
			add(instruction);
			add(empty);
			add(name);
			add(nameText);
			add(prenume);
			add(prenumeText);
			add(address);
			add(addressText);
			add(email);
			add(emailText);
			add(back);
			add(save);
			backListener();
			saveListener();
			
		}

		public void backListener() {
			back.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					frame.setVisible(false);
					
				}
			});
		}
		
		public void saveListener() {
			save.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					Customer customer = new Customer(nameText.getText(),prenumeText.getText(),emailText.getText(),addressText.getText());
					
					connection = MyConnection.ConnectDb();			        
					String sql = "insert into Customer(nume, prenume, email,adresa) values (?,?,?,?)";
			        try {
			            ps = connection.prepareStatement(sql);
			            ps.setString(1, customer.getName());
			            ps.setString(2, customer.getPrenume());
			            ps.setString(3, customer.getEmail());
			            ps.setString(4, customer.getAdress());
			            ps.execute();
			            JOptionPane.showMessageDialog(null, "Data Saved");
			            
			        } catch (SQLException e1) {
			            JOptionPane.showMessageDialog(null, e1);
			        }
				}
			});
		}
		
		

	}







