package Models;

public class Customer {

	
	private String name;
	private String prenume;
	private String email;
	private String adress;
	
	public Customer(String name,String prenume,String email,String adress) {
		
		this.setName(name);
		this.setPrenume(prenume);
		this.setEmail(email);
		this.setAdress(adress);
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrenume() {
		return prenume;
	}

	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}
	
	
	
}
