package Panels;

import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Connection.MyConnection;
import javafx.scene.layout.Border;

public class CustomerView extends JPanel{
	
	Connection connection = null;
    ResultSet rs = null;
    PreparedStatement ps = null;
	private JFrame frame;
	private JTable table;
	private JButton back;
    
    DefaultTableModel defaultTableModel = new DefaultTableModel();
    String data;
    
    public CustomerView() {
    	
    	frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		frame.getContentPane().setLayout(new GridLayout(1,1));
	    frame.setVisible(true);
	    setLayout(new GridLayout(2,1));
	    back = new JButton("Back");
	    back.setBounds(0, 10, 0, 22);
	    back.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frame.setVisible(false);
				
			}
		});
		Object columns[] = {"Id","Nume","Prenume","Email","Adresa","Utilizator ID"};
        defaultTableModel.setColumnIdentifiers(columns);
		
		table = new JTable();
		
		
		JScrollPane scrollPane = new JScrollPane();
		table.setModel(defaultTableModel);
		scrollPane.setViewportView(table);
		
		add(scrollPane);
        add(back);
        frame.add(this);
        loadData();
    
        
    }
	
	
	 public void loadData() {
	        connection = MyConnection.ConnectDb();
	        defaultTableModel.getDataVector().removeAllElements();
	        defaultTableModel.fireTableDataChanged();
	        String sql = "select * from Customer";
	        try {
	            ps = connection.prepareStatement(sql);
	            rs = ps.executeQuery();
	            Object columns[] = {"Id","Nume","Prenume","Email","Adresa"};
	            defaultTableModel.setColumnIdentifiers(columns);
	            Object columnData[] = new Object[6];
	            while (rs.next()) {
	                columnData[0] = rs.getInt("customer_id");
	                columnData[1] = rs.getString("nume");
	                columnData[2] = rs.getString("prenume");
	                columnData[3] = rs.getString("email");
	                columnData[4] = rs.getString("adresa");
	                defaultTableModel.addRow(columnData);

	            }
	        } catch (SQLException e) {
	            JOptionPane.showMessageDialog(null, e);
	        }
	    }
	 
	 
	 
	

}
