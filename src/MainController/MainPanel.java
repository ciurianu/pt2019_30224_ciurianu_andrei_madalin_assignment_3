package MainController;


import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

//import groups.ShopCustomers;

public class MainPanel extends JPanel {
	
	

	private static final long serialVersionUID = 1L;
	private JButton viewCustomers = new JButton("View Customers");
	private JButton viewWarehouse = new JButton("View Warehouse");
	private JButton viewOrders = new JButton("View Orders");
	private JButton addProduct = new JButton("Add Product");
	private JButton removeProduct = new JButton("Remove Product");
	private JButton updateStock = new JButton("Update Stock");
	private JButton addCostumer = new JButton("Add Customer");
	private JButton editCustomer = new JButton("Edit Customer");
	private JButton deleteCustomer = new JButton("Delete Customer");
	private JButton addOrder = new JButton("Add Order");
	private JFrame frame = new JFrame("Order Management");


	public  MainPanel() {
		
		frame.setSize(480, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		setLayout(new GridLayout(5, 2));

		add(viewCustomers);
		add(addCostumer);
		add(editCustomer);
		add(deleteCustomer);
		add(viewWarehouse);
		add(addProduct);
		add(removeProduct);
		add(updateStock);
		add(viewOrders);
		add(addOrder);
		setVisible(true);
		
		frame.add(this);
	}
	
	public void viewCustomers(final ActionListener actionListener) {
		viewCustomers.addActionListener(actionListener);
	}
	public void editCustomer(final ActionListener actionListener) {
		editCustomer.addActionListener(actionListener);
	}
	public void addCustomer(final ActionListener actionListener) {
		addCostumer.addActionListener(actionListener);
	}
	public void deleteCustomer(final ActionListener actionListener) {
		deleteCustomer.addActionListener(actionListener);
	}
	
	public void viewWarehouse(final ActionListener actionListener) {
		viewWarehouse.addActionListener(actionListener);
	}
	public void viewOrders(final ActionListener actionListener) {
		viewOrders.addActionListener(actionListener);
	}
	public void addProduct(final ActionListener actionListener) {
		addProduct.addActionListener(actionListener);
	}
	public void removeProduct(final ActionListener actionListener) {
		removeProduct.addActionListener(actionListener);
	}
	public void updateStock(final ActionListener actionListener) {
		updateStock.addActionListener(actionListener);
	}
	public void addOrder(final ActionListener actionListener) {
		addOrder.addActionListener(actionListener);
	}

}