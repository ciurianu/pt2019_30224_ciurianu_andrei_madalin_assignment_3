package Models;

public class Product {

	private float pret;
	private int stoc;
	private String nume;
	
	public Product(String nume,float pret, int stoc) {
		this.nume = nume;
		this.pret = pret;
		this.stoc = stoc;
		
	}
	
	public float getPret() {
		return pret;
	}

	public void setPret(float pret) {
		this.pret = pret;
	}

	public int getStoc() {
		return stoc;
	}

	public void setStoc(int stoc) {
		this.stoc = stoc;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	
	
}
