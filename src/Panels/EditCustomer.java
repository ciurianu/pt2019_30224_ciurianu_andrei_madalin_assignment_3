package Panels;

import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Connection.MyConnection;
import Models.Customer;
import javafx.scene.layout.Border;

public class EditCustomer extends JPanel{
	
	Connection connection = null;
    ResultSet rs = null;
    PreparedStatement ps = null;
	private JFrame frame;
	private JTable table;
	private JButton back;
	private JButton save;
	private Customer customer = null;
	private int idCustomer;
    
    DefaultTableModel defaultTableModel = new DefaultTableModel();
    String data;
    
    public EditCustomer() {
    	
    	frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		frame.getContentPane().setLayout(new GridLayout(1,1));
	    frame.setVisible(true);
	    setLayout(new GridLayout(3,2));
	    back = new JButton("Back");
	    save = new JButton("Save");
	    back.setBounds(0, 10, 0, 22);
	    back.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frame.setVisible(false);
				
			}
		});
		Object columns[] = {"Id","Nume","Prenume","Email","Adresa","Utilizator ID"};
        defaultTableModel.setColumnIdentifiers(columns);
		
		table = new JTable();
		
		table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TableMouseClicked(evt);
            }
        });
		JScrollPane scrollPane = new JScrollPane();
		table.setModel(defaultTableModel);
		scrollPane.setViewportView(table);
		
		add(scrollPane);
        add(back);
        add(save);
        frame.add(this);
        loadData();
        saveListener();
    
        
    }
	
	
	 public void loadData() {
	        connection = MyConnection.ConnectDb();
	        defaultTableModel.getDataVector().removeAllElements();
	        defaultTableModel.fireTableDataChanged();
	        String sql = "select * from Customer";
	        try {
	            ps = connection.prepareStatement(sql);
	            rs = ps.executeQuery();
	            Object columns[] = {"Id","Nume","Prenume","Email","Adresa"};
	            defaultTableModel.setColumnIdentifiers(columns);
	            Object columnData[] = new Object[6];
	            while (rs.next()) {
	                columnData[0] = rs.getInt("customer_id");
	                columnData[1] = rs.getString("nume");
	                columnData[2] = rs.getString("prenume");
	                columnData[3] = rs.getString("email");
	                columnData[4] = rs.getString("adresa");
	                defaultTableModel.addRow(columnData);

	            }
	        } catch (SQLException e) {
	            JOptionPane.showMessageDialog(null, e);
	        }
	    }
	 
	public void saveListener() {
		save.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				
				connection = MyConnection.ConnectDb();			        
				String sql = "Update Customer set nume = '" + customer.getName() +"' , prenume = '" + customer.getPrenume() +"' , email = '" + customer.getEmail() + "', adresa = '" +customer.getAdress()+ "'  where customer_id = '" + idCustomer + "'";
				try {
		            ps = connection.prepareStatement(sql);
		            ps.execute();
		            defaultTableModel.getDataVector().removeAllElements();
		            defaultTableModel.fireTableDataChanged();
		            loadData();
		            JOptionPane.showMessageDialog(null, "Data Updated");
		        } catch (HeadlessException | SQLException e1) {
		            JOptionPane.showMessageDialog(null, e1);
		        }
			}
		});
	}
	
	 private void TableMouseClicked(MouseEvent evt) {
	        int row = table.getSelectedRow();
	        idCustomer = Integer.parseInt(table.getValueAt(row, 0).toString());
	        String nume = table.getValueAt(row, 1).toString();
	        String prenume = table.getValueAt(row, 2).toString();
	        String email = table.getValueAt(row, 3).toString();
	        String adresa =table.getValueAt(row, 4).toString();
	        customer = new Customer(nume, prenume, email, adresa);
	       
	    }

}
