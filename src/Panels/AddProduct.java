package Panels;


import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Connection.MyConnection;
import Models.Product;

public class AddProduct extends JPanel {
	

	private JLabel instruction = new JLabel("Complete Product");
	private JLabel empty = new JLabel("");
	private JLabel name = new JLabel("Nume");
	private JTextField nameText = new JTextField();
	private JLabel pret = new JLabel("Pret");
	private JTextField pretText = new JTextField();
	private JLabel stoc = new JLabel("Stoc");
	private JTextField stocText = new JTextField();
	private JButton back = new JButton("Back");
	private JButton save = new JButton("Save");
	private JFrame frame;
	Connection connection = null;
    PreparedStatement ps = null;

		public  AddProduct() {
			frame = new JFrame();
			frame.setBounds(100, 100, 450, 300);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			 frame.setVisible(true);
			 frame.add(this);
			setLayout(new GridLayout(5, 2));
			add(instruction);
			add(empty);
			add(name);
			add(nameText);
			add(pret);
			add(pretText);
			add(stoc);
			add(stocText);
			add(back);
			add(save);
			backListener();
			saveListener();
			
		}

		public void backListener() {
			back.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					frame.setVisible(false);
					
				}
			});
		}
		
		public void saveListener() {
			save.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					//Customer customer = new Customer(nameText.getText(),prenumeText.getText(),emailText.getText(),addressText.getText());
					Product product = new Product(nameText.getText(),Float.parseFloat(pretText.getText().toString()),Integer.parseInt(stocText.getText().toString()));
					connection = MyConnection.ConnectDb();			        
					String sql = "insert into Product(descriere, pret, stoc) values (?,?,?)";
			        try {
			            ps = connection.prepareStatement(sql);
			            ps.setString(1, product.getNume());
			            ps.setFloat(2, product.getPret());
			            ps.setInt(3, product.getStoc());
			            ps.execute();
			            JOptionPane.showMessageDialog(null, "Data Saved");
			            
			        } catch (SQLException e1) {
			            JOptionPane.showMessageDialog(null, e1);
			        }
				}
			});
		}
		
		

	}







